package uz.pdp.appjpa.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.NaturalIdCache;

import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @OrderBy(value = "rowNumber, name, price")
    @OneToMany(mappedBy = "event", cascade = CascadeType.PERSIST)
    private List<Seat> seats;
}
