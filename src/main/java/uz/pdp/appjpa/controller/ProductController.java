package uz.pdp.appjpa.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appjpa.entity.Category;
import uz.pdp.appjpa.entity.Product;
import uz.pdp.appjpa.mapper.ProductMapper;
import uz.pdp.appjpa.payload.*;
import uz.pdp.appjpa.repository.CategoryRepository;
import uz.pdp.appjpa.repository.ProductRepository;
import uz.pdp.appjpa.repository.specification.ProductSpecification;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ProductMapper productMapper;

    @Transactional
    @PostMapping
    public HttpEntity<ProductDTO> add(@Valid @RequestBody ProductAddDTO productAddDTO) {

        Product product = productMapper.toProduct(productAddDTO);

        Category category = categoryRepository.findById(productAddDTO.getCategoryId()).orElseThrow(() -> new RuntimeException("OKa category yo'q: " + productAddDTO.getCategoryId()));
        product.setCategory(category);

        productRepository.save(product);

        List<Product> products = category.getProducts();
        if (products == null)
            products = new LinkedList<>();

        products.add(product);
        categoryRepository.save(category);

        ProductDTO productDTO = productMapper.toProductDTO(product);

        return ResponseEntity.status(HttpStatus.CREATED).body(productDTO);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> one(@PathVariable Integer id) {
        Product product = productRepository.findById(id).orElseThrow(RuntimeException::new);
        return ResponseEntity.ok(product);
    }

    @GetMapping
    public List<Product> list() {
        List<Product> all = productRepository.findAll();
        return all;
    }

    @PostMapping("/for-test")
    public HttpEntity<PaginationDTO<ProductDTO>> forTest(@RequestParam int page, @RequestParam int size, @RequestBody ProductSearchDTO searchDTO) {

        Pageable pageable = PageRequest.of(page, size);
        Page<Product> productPage;

        if (searchDTO.getName() != null) {
            productPage = productRepository.findAllByNameContainingIgnoreCase(searchDTO.getName(), pageable);
        } else if (searchDTO.getPrice() != null) {
            productPage = productRepository.findAllByPrice(searchDTO.getPrice(), pageable);
        } else if (searchDTO.getCategoryName() != null) {
            productPage = productRepository.findAllByCategory_NameContainingIgnoreCase(searchDTO.getCategoryName(), pageable);
        } else {
            productPage = productRepository.findAll(pageable);
        }

        List<ProductDTO> productDTOList = productMapper.toProductDTOList(productPage.getContent());
        PaginationDTO<ProductDTO> paginationDTO = PaginationDTO.simplePagination(size, page, productDTOList);

        return ResponseEntity.ok(paginationDTO);
    }

    @PostMapping("/for-test-specification")
    public HttpEntity<PaginationDTO<ProductDTO>> forTestSpecification(@RequestParam int page,
                                                                      @RequestParam int size,
                                                                      @RequestBody SearchCriteria searchCriteria) {

        Pageable pageable = PageRequest.of(page, size);


        ProductSpecification specification = new ProductSpecification(searchCriteria);

        Page<Product> productPage = productRepository.findAll(specification, pageable);

        List<ProductDTO> productDTOList = productMapper.toProductDTOList(productPage.getContent());
        PaginationDTO<ProductDTO> paginationDTO = PaginationDTO.simplePagination(size, page, productDTOList);

        return ResponseEntity.ok(paginationDTO);
    }
}
