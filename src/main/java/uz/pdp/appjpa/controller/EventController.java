package uz.pdp.appjpa.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uz.pdp.appjpa.entity.Event;
import uz.pdp.appjpa.entity.Seat;
import uz.pdp.appjpa.payload.EventAddDTO;
import uz.pdp.appjpa.payload.SeatRowDTO;
import uz.pdp.appjpa.repository.EventRepository;
import uz.pdp.appjpa.repository.SeatRepository;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/event")
@RequiredArgsConstructor
public class EventController {

    private final EventRepository eventRepository;
    private final SeatRepository seatRepository;

    @PostMapping
    public HttpEntity<?> add(@Valid @RequestBody EventAddDTO eventAddDTO) {

        Event event = Event.builder()
                .name(eventAddDTO.getName())
                .build();

        List<Seat> seats = new LinkedList<>();
        int rowNum = 1;
        for (SeatRowDTO rowDTO : eventAddDTO.getSeatRows()) {
            int count = rowDTO.getCount();
            for (int i = 0; i < count; i++) {
                Seat seat = Seat.builder()
                        .event(event)
                        .price(rowDTO.getPrice())
                        .rowNumber(rowNum)
                        .name(rowNum + "A")
                        .build();
                seats.add(seat);
            }
            rowNum++;
        }

        event.setSeats(seats);
        eventRepository.save(event);

        return ResponseEntity.ok("");
    }

    @Transactional
    @DeleteMapping("/{id}")
    public String delete(@PathVariable Integer id) {
        eventRepository.deleteById(id);
        return "OK";
    }

    @GetMapping("/{id}")
    public Event one(@PathVariable Integer id) {
        return eventRepository.findById(id).orElseThrow();
    }

    public static void main(String[] args) {
        String name = "1";

        for (int i = 0; i < 17345; i++) {
            int a = i;
            String str = "";
            while (a > 26) {
                str = ((char) (65 + (a % 26))) + str;
                a /= 26;
            }
            if (a > 0)
                str = ((char) (65 + a - 1)) + str;
            else {
                str = ((char) (65 + a)) + str;
            }
            System.out.println(name + str);
        }
    }
}
