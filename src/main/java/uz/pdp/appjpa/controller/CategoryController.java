package uz.pdp.appjpa.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appjpa.entity.Category;
import uz.pdp.appjpa.entity.Product;
import uz.pdp.appjpa.payload.CategoryDTO;
import uz.pdp.appjpa.payload.ProductDTO;
import uz.pdp.appjpa.repository.CategoryRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/category")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryRepository categoryRepository;

    @GetMapping("/{id}")
    public HttpEntity<Category> one(@PathVariable Integer id) {
        Category category = categoryRepository.findById(id).orElseThrow();

//
//        List<ProductDTO> productDTOList = category.getProducts().stream()
//                .map(product -> ProductDTO.builder()
//                        .id(product.getId())
//                        .name(product.getName())
//                        .price(product.getPrice())
//                        .build()).toList();
//
//        CategoryDTO categoryDTO = CategoryDTO.builder()
//                .id(category.getId())
//                .name(category.getName())
//                .products(productDTOList)
//                .build();


        return ResponseEntity.ok(category);
    }
}
