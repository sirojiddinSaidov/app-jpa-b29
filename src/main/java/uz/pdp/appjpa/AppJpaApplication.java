package uz.pdp.appjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//@EnableJpaRepositories(basePackages = "uz.pdp.appjpa")
public class AppJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppJpaApplication.class, args);
    }

}
