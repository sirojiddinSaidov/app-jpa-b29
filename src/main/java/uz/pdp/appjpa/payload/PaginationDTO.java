package uz.pdp.appjpa.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PaginationDTO<T> {

    private int totalPages;

    private int totalElements;

    private int size;

    private int number;

    private int numberOfElements;

    private List<T> content;

    private PaginationDTO(int size, int number, List<T> content) {
        this.size = size;
        this.number = number;
        this.content = content;
    }

    public static <E> PaginationDTO<E> simplePagination(int size, int number, List<E> content) {
        return new PaginationDTO<>(size, number, content);
    }


}
