package uz.pdp.appjpa.payload;

import lombok.Data;

@Data
public class SearchCriteria {

    private String column;

    private String operation;//>, >=, <, <=, EQ, NULL

    private Object value;
}
