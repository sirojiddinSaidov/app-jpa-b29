package uz.pdp.appjpa.payload;

import lombok.Data;

@Data
public class SeatRowDTO {

    private double price;

    private int count;
}
