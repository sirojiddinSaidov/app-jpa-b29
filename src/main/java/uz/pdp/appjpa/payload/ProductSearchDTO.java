package uz.pdp.appjpa.payload;

import lombok.Data;

@Data
public class ProductSearchDTO {

    private String name;

    private Double price;

    private String categoryName;
}
