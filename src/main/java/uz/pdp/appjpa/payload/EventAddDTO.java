package uz.pdp.appjpa.payload;


import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.List;

@Data
public class EventAddDTO {

    @NotBlank
    private String name;

    private List<SeatRowDTO> seatRows;


}
