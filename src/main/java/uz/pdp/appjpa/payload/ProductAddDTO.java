package uz.pdp.appjpa.payload;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;

import java.util.List;

@Data
public class ProductAddDTO {

    @NotBlank
    private String name;

    @Positive
    private double price;

    @NotNull
    private Integer categoryId;
}
