package uz.pdp.appjpa.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import uz.pdp.appjpa.entity.Product;
import uz.pdp.appjpa.payload.ProductAddDTO;
import uz.pdp.appjpa.payload.ProductDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    Product toProduct(ProductAddDTO productAddDTO);

    @Mapping(target = "category.products", ignore = true)
    ProductDTO toProductDTO(Product product);

//    @Mapping(target = "category.products", ignore = true)
    List<ProductDTO> toProductDTOList(List<Product> products);
}
