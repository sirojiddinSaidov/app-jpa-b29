package uz.pdp.appjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.appjpa.entity.Seat;

@Repository
public interface SeatRepository extends JpaRepository<Seat, Integer> {

    void deleteByEventId(Integer id);
}