package uz.pdp.appjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.appjpa.entity.Event;

public interface EventRepository extends JpaRepository<Event, Integer> {
}
