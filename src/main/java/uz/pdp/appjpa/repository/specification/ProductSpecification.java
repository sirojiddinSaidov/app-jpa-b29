package uz.pdp.appjpa.repository.specification;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestBody;
import uz.pdp.appjpa.entity.Product;
import uz.pdp.appjpa.payload.SearchCriteria;

@RequiredArgsConstructor
public class ProductSpecification implements Specification<Product> {

    private final SearchCriteria criteria;

    @Override
    public Predicate toPredicate
            (Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        if (criteria.getOperation().equals(">")) {
            return builder.greaterThan(
                    root.get(criteria.getColumn()), criteria.getValue().toString());
        } else if (criteria.getOperation().equals("<")) {
            return builder.lessThan(
                    root.get(criteria.getColumn()), criteria.getValue().toString());
        } else if (criteria.getOperation().equalsIgnoreCase(":")) {
            if (root.get(criteria.getColumn()).getJavaType() == String.class) {
                return builder.like(
                        root.get(criteria.getColumn()), "%" + criteria.getValue() + "%");
            } else {
                return builder.equal(root.get(criteria.getColumn()), criteria.getValue());
            }
        }
        return null;
    }
}
