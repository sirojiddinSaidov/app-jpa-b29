package uz.pdp.appjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.appjpa.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
}